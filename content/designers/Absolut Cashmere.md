---
title: Absolut Cashmere
excerpt: 'Originating in France Absolut Cashmere produces eco friendly sustainable
  knitwear from inner Mongolia.... '
descirption: 'Designed in Paris and made in Inner Mongolia Absolut Casmere offers
  luxurious , elegant, and sustainable knitwear in a rainbow wave of different colours.
  From classic ,slim fit crew necks to oversized boyfriend jumpers each piece is designed
  to be easy to wear and slot into your wardrobe with ease.  With exciting collaborations
  and developments in recycling cashmere and sustainability Absolut Cashmere always
  offers something fresh each season whether it s a new take on a simple linen t-shirt
  or a fashion forward Intarsia knit. '

---
